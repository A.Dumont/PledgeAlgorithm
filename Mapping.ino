#include "Mapping.h"
#include "string.h"


/*#######################################################################
 void squeeze(int j)
 
 Principe: Fonction de decalage de 1 vers la droite des valeurs d'un 
 tableau. La fonction parcourt le tableau et remplace chaque valeur 
 actuelle par la valeur a sa droite.
 ######################################################################## */

void squeeze(int j)
{
  while(j<TAILLETAB && path[j]!='\0') // tant qu'on a pas atteit la taille max et que la fin du tableau n'est pas atteinte
  {
    path[j]=path[j+1];
    j++;
  }
}


/*#######################################################################
 void simplify(int i)
 
 Principe: Fonction récursive qui simplifie le parcours enregistré du robot. 
 Pour ce faire, elle parcourt le tableau jusqu'à ce qu'elle trouve un B ou 
 arrive à la fin, puis si elle est sur un B, elle considere le caractère 
 précédent et le caractere suivantet remplace le B par la nouvelle direction.
 Elle appelle ensuite squeeze pour mettre à jour le tableau (supprimer les 
 deux cases qui viennent d'etre considérées pour la simplification) et se 
 rappelle elle mème sur le caractere suivant dans le tableau.
 B = demi tour
 R = virage a droit
 L = virage a gauche
 S = aller tout droit 
 ######################################################################## */

void simplify(int i)
{
  if(path[0]!='\0') 
  {
    while(path[i]!='\0' && path[i]!='B' || !i) // tant que i est différent de zero (utile si on a un B en debut de tableau) ou que le caractere n'est pas B ou fin de tableau
    {
      i++; //on avance dans le tableau
    }
    if(path[i]!='\0') // si on est sur un B
    { 
      if(path[i-1]=='R' && path[i+1]=='R') // on regarde le caractere précédent et le suivant
      {
        path[i]='S'; // on simplifie 
      }
      else if(path[i-1]=='L' && path[i+1]=='L') // de meme pour chaque cas
      {
        path[i]='S';
      }
      else if(path[i-1]=='R' && path[i+1]=='L')
      {
        path[i]='B';
      }
      else if(path[i-1]=='L' && path[i+1]=='R')
      {
        path[i]='B';
      }
      else if(path[i-1]=='S' && path[i+1]=='S')
      {
        path[i]='B';
      }
      else if(path[i-1]=='L' && path[i+1]=='S')
      {
        path[i]='R';
      }
      else if(path[i-1]=='S' && path[i+1]=='L')
      {
        path[i]='R';
      }
      else if(path[i-1]=='R' && path[i+1]=='S')
      {
        path[i]='L';
      }
      else if(path[i-1]=='S' && path[i+1]=='R')
      {
        path[i]='L';
      }

      squeeze(i-1); // decale tout le tableau a droite de i-1 de 1 vers la gauche
      squeeze(i); // decale tout le tableau a droite de i de 1 vers la gauche

      simplify(i-1); // apres 2 squeeze, la case appelée ici est en fait la suivante dans le tableau  (i+1)
    }
  }
}


/*#######################################################################
 void PrintMap()
 
 Principe: La fonction parcours le tableau path jusqu'a la fin, et envoie 
 les informations lues sur le periphérique connecté en bluetooth. Cette 
 fonction permet de vérifier ce que le robot a detecté et ensuite le 
 résultat de la simplification du parcours.
 ######################################################################## */

void printMap()
{
  int i=0;
  while(path[i]!='\0') // tant que la fin du tableau n'est pas atteinte
  {
    Serial.print(path[i]); // fonction de communication du langage arduino
    Serial.print('|');
    i++;
  }
}

