#ifndef MAPPING_H
#define MAPPING_H

#define TAILLETAB 100  // valeur de taille maximum attribuée au tableau path

void squeeze(int);

void simplify(int);

void printMap();

#endif
