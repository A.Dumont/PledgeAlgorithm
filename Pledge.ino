


#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include "LineSensorArray.h"
#include "Motors.h"
#include "Mapping.h"

#include <MeMCore.h>

MeDCMotor motor_9(9);
MeDCMotor motor_10(10);

MeLineFollowerArray lineFollowerArray(A1);
MeLineFollower lineFollower(2);
MeRGBLed rgbled(7, 7 == 7 ? 2 : 4);
MeBuzzer buzzer;


double speed; //Variable de vitesse

int counter; //Compteur de Pledge

char path[100]; //Tableau des chemins pris
int pathInd; //Indice de parcours de "path"

int i;

int frontLineSensors; //Valeur du capteur de ligne avant
int backLineSensors; //Valeur du capteur de ligne arrière

long straightTime;

int finished; //Booléen


/*#########################################################
delayStraight

Si le robot, après avoir détecté un croisement ou un T, va
tout droit alors il ajoute un 'S' dans "path". La fonction
oblige à attendre au moins 500ms avant d'en ajouter un autre.
Sinon le robot, le temps de parcourir tout le croisement, va
ajouter plusieurs fois 'S'.
#########################################################*/

void delayStraight()
{
  if(millis()-straightTime>500) //Si 500ms se sont écoulées depuis le dernier 'S'...
  {
    straightTime=millis(); //...l'origine des temps est avancée
    path[pathInd] = 'S'; //un 'S' est ajouté à path
    pathInd++;
  }
}


/*#######################################################################
voit setup()
 
 Principe: Foncton d'initialisation des valeurs des variables utilisées
 ######################################################################## */

void setup()
{

  straightTime=millis(); //Initialisation de l'origine des temps pour la fonction "delayStraight"
  counter=-1; //Conséquence de la decision que le robot colle le mur de gauche
  pathInd = 0;
  finished = 0;
  speed = 130;

  
  pinMode(A7, INPUT);
  rgbled.setColor(0, 0, 0, 0);
  rgbled.show();
  Serial.begin(115200);
  Serial.println("Hello");
  Wire.begin();
  i=0;
  
}


/*#######################################################################
voit loop()
 
 Principe: Fonction main qui régit l'intégralité du comportement du robot.
 ######################################################################## */

void loop()
{

  while(analogRead(A7) > 10); while(analogRead(A7) < 10); //Attente de l'appuie du bouton
  
  do //Tant que le parcours n'est pas fini 
  {
        frontLineSensors = lineFollower.readSensors(); //Lecture de la valeur du capteur de ligne avant
        backLineSensors = lineFollowerArray.getValue(); //Lecture de la valeur du capteur de ligne arrière
        

	/* Si le robot se trouve sur un virage, on le laisse parcourir la ligne un peu plus loin et on remesure
	   la valeur du capteur de ligne. Car dans le cas ou le robot n'arrive pas droit sur un "T vers le bas"
	   ou un croisement, il pourrait ne détecter qu'une partie de la ligne perpendiculaire et donc  il ne
	   détecterai qu'un simple virage à la place */
        if (backLineSensors == 3 || backLineSensors == 7 || backLineSensors == 48 || backLineSensors == 56)
        {
          long t0 = millis();
          while (millis() - t0 < 45);
          backLineSensors = lineFollowerArray.getValue();
        }
        

	/* Ce switch est la traduction de la machine à états représentant le robot. L'alternative est faite sur 
	   la valeur du capteur de ligne arrière. Pour chaque cas, la configuration du capteur sera schématisée
	   par une suite de 6 valeurs correspondant aux états de chaque petit capteur de ligne composant le capteur
	   arrière. Ces valeurs seront N si le petit capteur en question se trouve sur du noir, B si il se trouve
	   sur du blanc.
	   
	   						________________
						        |               |        
						        |               |        
						        |               |        
						        |               |        
						        |               |        
						        |               |        
						        |               |        
					              __| _____________ |__      
						     |  ||             ||  |      
						     |  || B|B|B|B|B|B ||  |   <- capteur de ligne arrière   
						     |  ||_____   _____||  |      
						      __-------| |-------__
						               |_|
*/
        switch (backLineSensors)
        {
          case 0: // N|N|N|N|N|N
            if (frontLineSensors == 0) //Croisement
            {
              if (counter)
              {
                turn('L');
                counter++;
                path[pathInd] = 'L';
                pathInd++;
              }
              else
              {
                delayStraight();
              }
            }
            else if (frontLineSensors == 3) //T vers le bas
            {
              turn(counter ? 'L' : 'R');
              path[pathInd] = (counter ? 'L' : 'R');
              counter += (counter ? 1 : -1);
              pathInd++;
            }
            
            break;
          case 3: // B|B|B|B|N|N
          case 7: // B|B|B|N|N|N
            if (frontLineSensors == 3) //Virage à droite
            {
              turn('R');
              counter--;
              path[pathInd] = 'R';
              pathInd++;
            }
            else //T à droite
            {
              delayStraight();
            }
            
            break;
          case 48: // N|N|B|B|B|B
          case 56: // N|N|N|B|B|B
            if (frontLineSensors != 3) //T à gauche
            {
              if (counter)
              {
                turn('L');
                counter++;
                path[pathInd] = 'L';
                pathInd++;
              }
              else
              {
                delayStraight();
              }
            }
            else //Virage à gauche
            {
              turn(counter ? 'L' : 'R');
              path[pathInd] = (counter ? 'L' : 'B');
              counter += (counter ? 1 : -2);
              pathInd++;
            }
            
            break;
          case 63: // B|B|B|B|B|B
            if (frontLineSensors == 3)  //Impasse
            {
              turn('R');
              counter -= 2;
              path[pathInd] = 'B';
            }
            pathInd++;
            break;
          case 18: // Arrivée N|B|N|N|B|N
              motor_9.stop(); //Le robot s'immobilise
              motor_10.stop();
              rgbled.setColor(0, 0, 50, 0); //Les deux LEDs sont éclairées en vert
              rgbled.show();
              path[pathInd] = 'F'; //Ajout d'un marqueur de fin à "path"
              finished = 1;
              break;
          default:
            followLine();
            break;
        }
        
  
   }while(!finished);

  Serial.println("Trajet parcouru :");
  printMap();
  simplify(0); //Simplification du chemin parcouru
  Serial.println("");
  Serial.println("Trajet simplifie :");
  printMap();

  while(analogRead(A7) > 10); while(analogRead(A7) < 10); //Attente de l'appuie du bouton

  rgbled.setColor(0, 0, 0, 0); //Extinction des LEDs
  rgbled.show();

  finished=0; //Le booléen est réinitialisé, il servira pour la suite
  pathInd = 0;

  /* Après la simplification, il ne reste qu'un seul 'B' au maximum et si il y en a un
     il se trouve forcément au début. Ce cas particulier est traité en premier lieu */
  if(path[0]=='B') 
  {
    turn('R');
    pathInd++;
  }
    
  while(!finished)
  {
    frontLineSensors = lineFollower.readSensors();

    do //Tant que le robot est sur une ligne droite (en prenant en compte les éventuels décalages que le capteur pourrait avoir sur la ligne)...
    {
      followLine(); //...il avance
      frontLineSensors = lineFollower.readSensors();
      backLineSensors = lineFollowerArray.getValue();
    }while(backLineSensors == 51 || backLineSensors == 35 || backLineSensors == 49 || backLineSensors == 39 || backLineSensors == 57 || backLineSensors == 55 || backLineSensors == 59); // tant que le robot est sur de la ligne droite (plusieurs valeurs pour avoir de la marge sur la postion des capteurs)

    switch(path[pathInd])
    {
      case 'S': //Si le robot doit aller tout droit, il avance
        pushBot();
        break;
      case 'R': //Si il doit tourner dans une direction, alors il tourne
      case 'L':
        turn(path[pathInd]);
        pushBot();
        break;
      case 'F': //Si le robot a parcouru tout le chemin, le booléen "finished" prend la valeur 1
        finished=1;
        break;
    }
    pathInd++;
    
  }

  motor_9.stop(); //Le robot s'immobilise
  motor_10.stop();
  rgbled.setColor(0, 0, 50, 0); //Les LEDs s'éclairent en vert
  rgbled.show();
}



