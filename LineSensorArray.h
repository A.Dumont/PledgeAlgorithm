#ifndef LINE_SENSOR_ARRAY
#define LINE_SENSOR_ARRAY

#include <inttypes.h>
#include "MePort.h"

class MeLineFollowerArray: public MePort
{
  public:
    MeLineFollowerArray(int pin);
    uint8_t getValue();
  private:
    int _pin;
    int _arrayValue;
};


#endif
