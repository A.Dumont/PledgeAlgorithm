#include "Motors.h"


/*#######################################################################
 void move(int direction, int speed)
 
 Principe: la fonction met à jour la vitesse de rotation de chaque moteur
 afin de se diriger dans la direction demandée. Les vitesses de rotations
 sont de valeur opposée, ce qui permet au robot de tourner sur lui mème.
 ######################################################################## */
 
void move(int direction, int speed)
{
      int leftSpeed = 0;
      int rightSpeed = 0;
      if(direction == 1){ // Vers l'avant
          leftSpeed = speed;
          rightSpeed = speed;
      }else if(direction == 2){ // Vers l'arrière
          leftSpeed = -speed;
          rightSpeed = -speed;
      }else if(direction == 3){ // Vers la gauche
          leftSpeed = -speed;
          rightSpeed = speed;
      }else if(direction == 4){ // Vers la droite
          leftSpeed = speed;
          rightSpeed = -speed;
      }
      motor_9.run((9)==M1?-(leftSpeed):(leftSpeed)); // Fonction préexistante tirée de Scratch
      motor_10.run((10)==M1?-(rightSpeed):(rightSpeed)); // Fonction préexistante tirée de Scratch
}


/*#######################################################################
 void turn(char dir)
 
 Principe: la fonction est appelée avec dir = R ou dir = L (correspond à 
 droite ou gauche), puis appelle move avec respectivement 4 ou 3 et 
 ensuite speed  pour faire tourner le robot, jusqu'a ce que les capteurs 
 avant retombent  sur une ligne noire, puis appelle pushBot.
 ######################################################################## */
 
void turn(char dir)
{
  switch(dir)
  {
    case 'R':
      move(4, speed);
      break;
    case 'L':
      move(3,speed);
      break;
  }

  while(lineFollower.readSensors()!=3); // tant que les deux capteurs avant ne sont pas sur du blanc
  while(lineFollower.readSensors()==3); // tant que les deux capteurs avant sont sur du blanc (on retombe donc sur une ligne noire
  pushBot();
}


/*#######################################################################
 void followLine()
 
 Principe: Fonction de suivi de ligne basique. 
        Si la valeur des capteurs avant est 1 : on tourne vers la droite
                                            2 : on tourne vers la gauche
        Sinon : on avance tout droit.
 Pour faire tourner le robot, on donne une vitesse de rotation a un moteur
 supérieure a celle de l'autre moteur.
 ######################################################################## */
 
void followLine()
{
      if(frontLineSensors==1) // si le capteur de gauche du robot est sur le blanc, donc robot trop a gauche
      {
        motor_9.run((9)==M1?-(80):(80)); // Fonction préexistante tirée de Scratch
        motor_10.run((10)==M1?-(130):(130));
      }
      else if(frontLineSensors==2)  // si le capteur de droite du robot est sur le blanc, donc robot trop a droite
      {
        motor_9.run((9)==M1?-(130):(130)); // Fonction préexistante tirée de Scratch
        motor_10.run((10)==M1?-(80):(80));
      }
      else
      {
        move(1, speed);
      }
}


/*#######################################################################
 void pushBot()
 
 Principe: Cette fonction est appelée de nombreuses fois dans le code, et
 sert a imposer un followLine pendant 500 millisecondes, afin d'éviter que
 le robot ne détecte deux fois la même pièce.
 ######################################################################## */
 
void pushBot()
{
  long t0=millis(); // on initialise le chronomètre
  while(millis()-t0<500) // tant qu'il ne s'est pas écoulé 500 millisecondes
  {
    frontLineSensors = lineFollower.readSensors(); // on met à jour les capteurs avants
    followLine(); // suivi de ligne
  }
}

